# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

echo "=== running asetup ==="
# fancy pants check to get version number from ci image
DOCKER_ANALYSIS_BASE_VERSION=$(
    grep 'analysisbase' ${BASH_SOURCE[0]%/*}/.gitlab-ci.yml | sed -r 's/.*:([^-]*).*/\1/')
# let zsh people also use fancy pants
if [ -z "${DOCKER_ANALYSIS_BASE_VERSION}" ]; then
    DOCKER_ANALYSIS_BASE_VERSION=$(
        grep 'analysisbase' $( dirname ${(%):-%x} )/.gitlab-ci.yml | sed -r 's/.*:([^-]*).*/\1/')
fi
# make sure our pants weren't _too_ fancy
if [[ ! $DOCKER_ANALYSIS_BASE_VERSION =~ [0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "couldn't parse CI file for base image, setting up latest"
    unset DOCKER_ANALYSIS_TOP_VERSION
fi
asetup AnalysisBase,${DOCKER_ANALYSIS_BASE_VERSION-"21.2,latest"}
