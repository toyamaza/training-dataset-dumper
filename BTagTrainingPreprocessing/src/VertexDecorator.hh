#ifndef VERTEX_DECORATOR_HH
#define VERTEX_DECORATOR_HH

#include "xAODJet/Jet.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class VertexDecorator
{
public:
  VertexDecorator(const std::string& decorator_prefix = "example_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<float> m_deco;
};

#endif
