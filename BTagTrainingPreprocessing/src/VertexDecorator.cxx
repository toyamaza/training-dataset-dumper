#include "VertexDecorator.hh"

// the constructor just builds the decorator
VertexDecorator::VertexDecorator(const std::string& prefix):
  m_deco(prefix + "decorator")
{
}

// this call actually does the work on the jet
void VertexDecorator::decorate(const xAOD::Jet& jet) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = jet.btagging();
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }
  int ntrkj = -1;
  int ntrkv = -1;
  int n2t = -1;
  float pt = -99;
  float m = -99;
  float efc = -99;
  float ndist = -99;
  float sig3d = -99;
  float pb = -99;
  float pc = -99;
  float pu = -99;
  float llr = -99;
  int hasVtx=0;
  float vX=0;
  float vY=0;
  float vZ=0;
  const std::vector<ElementLink<xAOD::VertexContainer > > SV1vertices = btag->auxdata<std::vector<ElementLink<xAOD::VertexContainer > > >("SV1_vertices");
  if (SV1vertices.size() != 0) {
    btag->taggerInfo(ntrkj, xAOD::SV1_NGTinJet);
    btag->taggerInfo(ntrkv, xAOD::SV1_NGTinSvx);
    btag->taggerInfo(n2t  , xAOD::SV1_N2Tpair );
    btag->taggerInfo(m    , xAOD::SV1_masssvx );
    btag->taggerInfo(efc  , xAOD::SV1_efracsvx);
    btag->taggerInfo(ndist, xAOD::SV1_normdist);
    try {
      btag->variable<float>("SV1", "significance3d" , sig3d);
    } catch(...) {
      std::cout << " ERROR: 3dsignificance not available for SV1 .... PLEASE CHECK!!!" << std::endl;
    }
    pb=btag->SV1_pb();
    pc=btag->SV1_pc();
    pu=btag->SV1_pu();
    llr=btag->SV1_loglikelihoodratio();
    hasVtx=true;
    if ( SV1vertices.at(0).isValid() ) {
      const xAOD::Vertex *tmpVertex = *(SV1vertices.at(0));
      vX=tmpVertex->x();
      vX=tmpVertex->y();
      vX=tmpVertex->z();
    }
  }

  typedef ElementLink<xAOD::TrackParticleContainer> TrackLink;
  typedef std::vector<TrackLink> TrackLinks;

  TrackLinks SV1Tracks;
  SV1Tracks = btag->SV1_TrackParticleLinks();
  int ntrk = SV1Tracks.size();
  std::cout << ntrk << " " << ntrkv << std::endl;
  for (unsigned int i = 0; i < ntrk; i++) {
    const xAOD::TrackParticle *trk = *(SV1Tracks.at(i));
    // std::cout << trk->pt() << std::endl;
  }
  
  // Store something to the jet.
  m_deco(*btag) = std::log(jet.pt());
}
