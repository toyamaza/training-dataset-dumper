#include "DecoratorExample.hh"

// the constructor just builds the decorator
DecoratorExample::DecoratorExample(const std::string& prefix):
  m_deco(prefix + "decorator")
{
}

// this call actually does the work on the jet
void DecoratorExample::decorate(const xAOD::Jet& jet) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = jet.btagging();
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }

  // Store something to the jet.
  m_deco(*btag) = std::log(jet.pt());
}
