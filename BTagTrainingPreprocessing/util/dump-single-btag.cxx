#include <cstddef>
#include <memory>
#include <cmath>

#include "SingleBTagConfig.hh"

#include "BTaggingWriterConfiguration.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"
#include "SingleBTagOptions.hh"
#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "TrackSelector.hh"
#include "DecoratorExample.hh"
#include "TruthTools.hh"
#include "VertexDecorator.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackAugmenter.h"
#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "PathResolver/PathResolver.h"

#include "H5Cpp.h"
#include "nlohmann/json.hpp"

#include "TFile.h"
#include "TTree.h"

namespace {

  // sort functions
  bool by_d0(const xAOD::TrackParticle* t1,
             const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
    return std::abs(d0(*t1)) > std::abs(d0(*t2));
  }
  bool by_sd0(const xAOD::TrackParticle* t1,
              const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
    static SG::AuxElement::ConstAccessor<float> d0s("btag_ip_d0_sigma");
    return std::abs(d0(*t1) / d0s(*t1)) > std::abs(d0(*t2) / d0s(*t2));
  }
  bool by_signed_d0(const xAOD::TrackParticle* t1,
              const xAOD::TrackParticle* t2) {
    static SG::AuxElement::ConstAccessor<float> d0_signed("IP3D_signed_d0_significance");
    return d0_signed(*t1) > d0_signed(*t2);
  }

  typedef bool (*TrackSort)(const xAOD::TrackParticle* t1,
                            const xAOD::TrackParticle* t2);
  TrackSort trackSort(TrackSortOrder order) {
    switch(order) {
    case TrackSortOrder::ABS_D0_SIGNIFICANCE: return &by_sd0;
    case TrackSortOrder::ABS_D0: return &by_d0;
    case TrackSortOrder::D0_SIGNIFICANCE: return &by_signed_d0;
    default: throw std::logic_error("undefined sort order");
    }
  }

  std::vector<std::string> get(const VariableList& v, const std::string& k) {
    if (v.count(k)) return v.at(k);
    return {};
  }

}

int main (int argc, char *argv[]) {
  using FlavorTagDiscriminants::DL2HighLevel;
  SingleTagIOOpts opts = get_single_tag_io_opts(argc, argv);
  const SingleBTagConfig jobcfg = get_singlebtag_config(opts.config_file_name);
  // The name of the application:
  const char *const APP_NAME = "BTagTestDumper";

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");

  if (jobcfg.do_calibration){
    RETURN_CHECK( APP_NAME, calib_tool.setProperty("JetCollection", jobcfg.jet_calibration_collection) );
    RETURN_CHECK( APP_NAME, calib_tool.setProperty("ConfigFile", jobcfg.jet_calib_file) );
    RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibSequence", jobcfg.cal_seq) );
    RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibArea", jobcfg.cal_area) );
    RETURN_CHECK( APP_NAME, calib_tool.setProperty("IsData", false) );
    RETURN_CHECK( APP_NAME, calib_tool.initialize() );
  }

  JetCleaningTool jetcleaningtool("JetCleaningTool", JetCleaningTool::LooseBad, false);
  RETURN_CHECK( APP_NAME, jetcleaningtool.initialize() );

  JetVertexTaggerTool jvttool("JetVertexTaggerTool");
  RETURN_CHECK( APP_NAME, jvttool.initialize() );

  // this tool adds variables we need for b-tagging which are derived
  // from the jet
  TrackSelector track_selector(jobcfg.track_select_cfg);
  BTagJetAugmenter jet_augmenter;
  BTagTrackAugmenter track_augmenter;
  FlavorTagDiscriminants::BTagMuonAugmenter muon_augmenter("Muons");

  // this is just an example augmenter, it doesn't do anything important
  DecoratorExample example_decorator;

  VertexDecorator vertex_decorator;

  // configure sort function
  TrackSort track_sort = trackSort(jobcfg.track_sort_order);

  // new way to do output files
  H5::H5File output(opts.out, H5F_ACC_TRUNC);
  // set up jet writer
  BTagJetWriterConfig jet_cfg;
  jet_cfg.write_event_info = true;
  jet_cfg.char_variables = get(jobcfg.btag, "chars");
  jet_cfg.jet_int_variables = get(jobcfg.btag, "jet_int_variables");
  jet_cfg.jet_float_variables = get(jobcfg.btag,"jet_floats");
  jet_cfg.int_as_float_variables = get(jobcfg.btag, "ints_as_float");
  jet_cfg.float_variables = get(jobcfg.btag, "floats");
  jet_cfg.double_variables = get(jobcfg.btag, "doubles");
  jet_cfg.variable_maps.replace_with_defaults_checks = cfg::check_map_from(cfg::BTagDefaultsMap);
  jet_cfg.variable_maps.rename = {}; // please don't use this :(
  jet_cfg.name = "jets";
  // configure RNN
  std::vector<DL2HighLevel> dl2s;
  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    std::string res_path = PathResolverFindCalibFile(cfg.nn_file_path);
    std::cout << "loading " << res_path << std::endl;
    dl2s.emplace_back(res_path, FlipTagConfig::STANDARD, cfg.output_remapping);
  }

  BTagJetWriter jet_writer(output, jet_cfg);
  // set up track writer
  BTagTrackWriterConfig track_cfg;
  track_cfg.name = "tracks";
  track_cfg.uchar_variables = get(jobcfg.track, "uchar");
  track_cfg.int_variables = get(jobcfg.track, "ints");
  track_cfg.float_variables = get(jobcfg.track, "floats");
  track_cfg.output_size = {jobcfg.n_tracks_to_save};
  std::unique_ptr<BTagTrackWriter> track_writer(nullptr);
  if (opts.save_tracks && jobcfg.n_tracks_to_save > 0) {
    track_writer.reset(new BTagTrackWriter(output, track_cfg));
  }

  size_t n_truth_records_read = 0;
  size_t n_truth_record_errors = 0;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {
    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // JVT decorator
    SG::AuxElement::Decorator<float> dec_jvt("bTagJVT");
    // jetPtRank decorator
    SG::AuxElement::Decorator<int> dec_jet_rank("jetPtRank");
    // vr_overlap decorator
    SG::AuxElement::Decorator<int> dec_jet_overlap("vr_overlap");

    // Loop over its events:
    unsigned long long entries = event.getEntries();
    if (opts.max_events > 0) entries = std::min(opts.max_events, entries);
    for (unsigned long long entry = 0; entry < entries; ++entry) {

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }

      const xAOD::JetContainer *jets = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(jets, jobcfg.jet_collection) );

      const xAOD::TruthParticleContainer *tpc = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(tpc, "TruthParticles") );
      std::vector<const xAOD::TruthParticle*> truth_particles(
        tpc->begin(), tpc->end());

      std::vector<const xAOD::TruthParticle*> truth_leptons;
      try {
        truth_leptons = truth::getLeptonsFromWZ(truth_particles);
        n_truth_records_read++;
      } catch (truth::TruthRecordError& err) {
        n_truth_record_errors++;
        std::cerr << "Truth Record Error: " << err.what() << std::endl;
        continue;
      }

      const xAOD::EventInfo *event_info = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      // overlap removal of VR track jets using the recommendations from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTagCalib2017
      std::vector<TLorentzVector> v_jet_4vec;
      std::vector<float> v_jet_r;
      if (jobcfg.vr_cuts){
        for (const xAOD::Jet *const uncalib_jet : *jets) {
          // no need to use calibrated jets here since VR jets are not calibrated
          if (uncalib_jet->pt() < 5000 || std::abs(uncalib_jet->eta()) > 2.5) continue;
          if (uncalib_jet->numConstituents()<2) continue;
          float radius = std::max(0.02, std::min(0.4, 30e3 / (uncalib_jet->pt())));
          TLorentzVector jet_4vec;
          jet_4vec.SetPtEtaPhiE(uncalib_jet->pt(), uncalib_jet->eta(), uncalib_jet->phi(), uncalib_jet->e());
          v_jet_4vec.push_back(jet_4vec);
          v_jet_r.push_back(radius);
        }
      }

      unsigned int rank = 0;
      for (const xAOD::Jet *const uncalib_jet : *jets) {

        // this is just applying example decorations, for no reason
        // other than to show that they work.
        example_decorator.decorate(*uncalib_jet);

        vertex_decorator.decorate(*uncalib_jet);

        // this is more important stuff
        if (jobcfg.run_augmenters) {
          jet_augmenter.augment(*uncalib_jet, *uncalib_jet);
          muon_augmenter.augment(*uncalib_jet);
        } else {
          const xAOD::BTagging* btag = uncalib_jet->btagging();
          if (!btag) throw std::runtime_error("btagging object missing");
          jet_augmenter.augmentBtagJes(*uncalib_jet, *uncalib_jet);
        }
        for (const auto& dl2: dl2s) {
          dl2.decorate(*uncalib_jet);
        }

        std::unique_ptr<xAOD::Jet> calib_jet(new xAOD::Jet(*uncalib_jet));
        if (jobcfg.do_calibration){
          calib_tool.applyCalibration(*calib_jet);
        }
        if (truth::is_overlaping_lepton(*calib_jet, truth_leptons, 0.3)) {
          continue;
        }

        dec_jet_rank(*calib_jet) = rank++;
        if (jobcfg.do_calibration){
          float updated_jvt_value= jvttool.updateJvt(*calib_jet);
          dec_jvt(*calib_jet) = updated_jvt_value;
          if (calib_jet->pt() > 20000 && calib_jet->pt() < 60000 && std::abs(calib_jet->eta()) < 2.4 && updated_jvt_value < jobcfg.jvt_cut) continue;
          if ( ! jetcleaningtool.keep(*calib_jet)) continue;
        }
        else {
          dec_jvt(*calib_jet) = NAN;
          if (!jobcfg.vr_cuts){
            if (calib_jet->pt() > 20000 && calib_jet->pt() < 60000 && std::abs(calib_jet->eta()) < 2.4) continue;
          }
        }

        if (calib_jet->pt() < jobcfg.pt_cut || std::abs(calib_jet->eta()) > 2.5) {
          continue;
        }

        if (jobcfg.vr_cuts){
          if (calib_jet->numConstituents()<2) continue;
          int overlap = 0;
          TLorentzVector calib_jet_4vec;
          calib_jet_4vec.SetPtEtaPhiE(calib_jet->pt(), calib_jet->eta(), calib_jet->phi(), calib_jet->e());
          float radius = std::max(0.02, std::min(0.4, 30e3 / (calib_jet->pt())));
          for (unsigned int jet_j = 0; jet_j < v_jet_4vec.size(); jet_j++) {
            if (calib_jet_4vec == v_jet_4vec[jet_j]) continue;
            if (calib_jet_4vec.DeltaR(v_jet_4vec[jet_j]) < std::min(v_jet_r[jet_j], radius)){
              overlap = 1;
              break;
            }
          }
          dec_jet_overlap(*calib_jet) = overlap;
        }
        jet_writer.write(*calib_jet, event_info);

        if (track_writer) {
          auto tracks = track_selector.get_tracks(*uncalib_jet);
          for (const auto& track: tracks) {
            track_augmenter.augment(*track, *uncalib_jet);
          }
          sort(tracks.begin(), tracks.end(), track_sort);
          track_writer->write(tracks, *uncalib_jet);
        }
      }
    }
  }

  nlohmann::json metadata;
  metadata["n_truth_records_read"] = n_truth_records_read;
  metadata["n_truth_record_errors"] = n_truth_record_errors;
  std::ofstream metastream("userJobMetadata.json");
  metastream << metadata.dump(2);

  return 0;
}
