#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

namespace {
  TrackSortOrder get_sort_order(const boost::property_tree::ptree& pt,
                                const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }
}

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name) {
  namespace fs = boost::filesystem;
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;
  SingleBTagConfig config;

  pt::ptree cfg;
  pt::read_json(config_file_name, cfg);

  config.do_calibration = cft::boolinate(cfg, "do_calibration");
  if (config.do_calibration) {
    config.jet_calibration_collection = cfg.get<std::string>(
      "jet_calibration_collection");
  }
  config.run_augmenters = cft::boolinate(cfg, "run_augmenters");
  config.vr_cuts = cft::boolinate(cfg, "vr_cuts");
  config.jet_collection = cfg.get<std::string>("jet_collection");

  if(config.do_calibration){
  config.jet_calib_file = cfg.get<std::string>("jet_calib_file");
  config.cal_seq = cfg.get<std::string>("cal_seq");
  config.cal_area = cfg.get<std::string>("cal_area");
  config.jvt_cut = cfg.get<float>("jvt_cut");
  }

  config.pt_cut = cfg.get<float>("pt_cut");
  
  const pt::ptree& tracksel = cfg.get_child("track_selection");
  config.track_select_cfg.pt_minumum = tracksel.get<float>("pt_minimum");
  config.track_select_cfg.d0_maximum = tracksel.get<float>("d0_maximum");
  config.track_select_cfg.z0_maximum = tracksel.get<float>("z0_maximum");
  config.track_select_cfg.si_hits_minimum = tracksel.get<int>("si_hits_minimum");
  config.track_select_cfg.si_holes_maximum = tracksel.get<int>("si_holes_maximum");
  config.track_select_cfg.pix_holes_maximum = tracksel.get<int>("pix_holes_maximum");



  config.n_tracks_to_save = cfg.get<size_t>("n_tracks_to_save");
  config.track_sort_order = get_sort_order(cfg,"track_sort_order");

  // optional configuration
  for (const auto& nnpt: cfg.get_child("dl2_configs")) {
    config.dl2_configs.push_back(cft::get_dl2_config(nnpt.second));
  }

  // combine variable files if there are any file paths specified
  for (auto& node: cfg.get_child("variables")) {
    cft::combine_files(node.second, fs::path(config_file_name));
  }

  config.btag = cft::get_variable_list(cfg.get_child("variables.btag"));
  config.track = cft::get_variable_list(cfg.get_child("variables.track"));

  return config;
}
